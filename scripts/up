#!/usr/bin/bash

columns=$(tput cols)

blue_font=$(tput setaf 4)
orange_font=$(tput setaf 3)
normal=$(tput sgr0)

function repeatCharacter() {
  character=$1
  repeats=$2

  printf "$character%.0s" $(seq 1 "$repeats")
}

# Headers are defined titles with repeated characters on either side.
# Each additional header level takes up half as much screen.
# E.g. header level 1 is half the screen, level 2 is a fourth and so on.
function header() {
  headerTitle=" $1 "
  headerLevel=$2
  headerCharacter=$3

  fullLength=$((columns / headerLevel))
  headerTitleLength=${#headerTitle}

  remainingLength=$((fullLength - headerTitleLength))

  leftMarkerLength=$((remainingLength / 2))
  rightMarkerLength=$leftMarkerLength
  if ((remainingLength % 2 != 0)); then
    rightMarkerLength=$((leftMarkerLength + 1))
  fi

  printf "%s" "$blue_font"
  repeatCharacter "$headerCharacter" $leftMarkerLength
  printf "%s" "$orange_font"
  printf "%s" "$headerTitle"
  printf "%s" "$blue_font"
  repeatCharacter "$headerCharacter" $rightMarkerLength
  echo -e "$normal"
}

header "DNF" 2 "="
sudo dnf upgrade -y
header "Flatpak" 2 "="
flatpak update -y
header "Brew" 2 "="
brew update
brew upgrade

header "RustUp" 2 "="
rustup update
header "Cargo" 2 "="
cargo install-update -a

header "NodeJS" 2 "="
# NVM isn't in the path, so we must run its init script
[ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] && \. "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"
nvm install 'lts/*' --latest-npm --reinstall-packages-from=current
header "NPM" 2 "="
npm up -g
header "CorePack" 2 "="
corepack prepare pnpm@latest
header "PNPM" 2 "="
pnpm up -g

header "NeoVim-Config" 2 "="
git -C ~/.config/nvim pull
header "NeoVim-Config-Update" 2 "="
pip install -Ur ~/.local/share/nvim-update/nvim.requirements.txt
